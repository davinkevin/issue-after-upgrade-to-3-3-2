package com.gitlab.davinkevin.issue.projectreactor.issueafterupgradeto332

import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration.*
import java.time.Instant

class TokenProvider(
        key: String,
        clientId: String,
        private val webClient: WebClient,
        connectOnConstruction: Boolean = false
) {

    private val log = LoggerFactory.getLogger(TokenProvider::class.java)

    val tokenFlux = fetchTokenWithApiKey(AuthInformation(api_key = key, grant_type = "api_key", client_id = clientId))
            .log("tokenFlux")
            .replay(1)

    init {
        if (connectOnConstruction) {
            tokenFlux.connect()
        }
    }

    fun fetchTokenWithApiKey(info: AuthInformation): Flux<AuthJwtToken> {
        return fetchToken(info)
                .expand { fetchTokenAfterMidLife(info, it) }

                .doOnNext { log.info("Token fetched with success") }
                .doOnError { log.error("Fail to fetch token", it) }
                .doOnNext { log.debug("Token is ${it.access_token}") }

                .retryWhen { it.flatMap { Mono.delay(ofSeconds(1)) } }
                .log("fetchTokenWithApiKey")
    }

    private fun fetchTokenAfterMidLife(authInformation: AuthInformation, token: AuthJwtToken): Mono<AuthJwtToken> {
        return Mono.delay(between(Instant.now(), token.midLife))
                .flatMap { fetchToken(authInformation) }
                .log("fetchTokenAfterMidLife")
                .doOnSubscribe {
                    log.info("Waiting ${between(Instant.now(), token.midLife).seconds} seconds")
                    log.debug("with token being ${token.access_token}")
                }
    }

    private fun fetchToken(authInformation: AuthInformation): Mono<AuthJwtToken> {
        return webClient
                .post()
                .uri("/auth/with/api/key")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromFormData(authInformation.toFormData()))
                .retrieve()
                .bodyToMono<AuthJwtToken>()
                .doOnError { log.error("💥 Error during fetch of token ", it) }
                .doOnNext { log.info("🎉 token fetched") }
                .doOnNext { log.debug("📦 token is: $it") }
                .retryBackoff(Long.MAX_VALUE, ofSeconds(1), ofMinutes(1))
                .doOnSubscribe { log.info("Fetch token 🚀") }
    }
}

data class AuthInformation(val api_key: String, val grant_type: String, val client_id: String) {
    fun toFormData() = LinkedMultiValueMap<String, String>().apply {
        add("apikey", api_key)
        add("grant_type", grant_type)
        add("client_id", client_id)
    }
}

data class AuthJwtToken(val access_token: String, val expires_in: Int) {
    private val issuedAt: Instant = Instant.now()
    private val expirationDate: Instant = Instant.now().plusSeconds(expires_in.toLong())
    val midLife: Instant = issuedAt.plus(between(issuedAt, expirationDate).dividedBy(2))
}

