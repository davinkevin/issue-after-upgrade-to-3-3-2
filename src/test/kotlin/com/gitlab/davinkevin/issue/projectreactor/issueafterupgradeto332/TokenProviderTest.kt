package com.gitlab.davinkevin.issue.projectreactor.issueafterupgradeto332

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.okJson
import com.github.tomakehurst.wiremock.client.WireMock.post
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.stubbing.Scenario
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Timeout
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration.ofSeconds

class TokenProviderTest {

    private val server: WireMockServer = WireMockServer(WireMockConfiguration.wireMockConfig().port(9876))
            .apply {
                stubFor(post("/auth/with/api/key")
                        .inScenario("renew token after midlife")
                        .whenScenarioStateIs(Scenario.STARTED)
                        .willReturn(okJson("""{ "access_token": "1st", "expires_in": 10 }"""))
                        .willSetStateTo("After first fetch")
                )
                stubFor(post("/auth/with/api/key")
                        .inScenario("renew token after midlife")
                        .whenScenarioStateIs("After first fetch")
                        .willReturn(okJson("""{ "access_token": "2nd", "expires_in": 15 }"""))
                        .willSetStateTo("After second fetch")
                )
                stubFor(post("/auth/with/api/key")
                        .inScenario("renew token after midlife")
                        .whenScenarioStateIs("After second fetch")
                        .willReturn(okJson("""{ "access_token": "3rd", "expires_in": 20}"""))
                )
            }

    @BeforeEach fun beforeEach() = server.start()
    @AfterEach fun afterEach() = server.stop()

    private val client = WebClient.builder().baseUrl("http://localhost:9876/").build()

    @Test
    @Timeout(5)
    fun `should renew token after midlife by construction of the token provider with connect on init`() {
        /* Given */
        /* When */
        StepVerifier.withVirtualTime {
            TokenProvider("foo", "clientId", client, true)
                    .tokenFlux
                    .take(3)
        }
                /* Then */
                .expectSubscription()
                .assertNext { assertThat(it.access_token).isEqualTo("1st") }
                .thenAwait(ofSeconds(5))
                .assertNext { assertThat(it.access_token).isEqualTo("2nd") }
                .thenAwait(ofSeconds(15))
                .assertNext { assertThat(it.access_token).isEqualTo("3rd") }
                .verifyComplete()
    }

    @Test
    @Timeout(5)
    fun `should renew token after midlife with direct call`() {
        /* Given */
        val tokenProvider = TokenProvider("foo", "clientId", client, false)

        /* When */
        StepVerifier.withVirtualTime {
            tokenProvider
                    .fetchTokenWithApiKey(AuthInformation(api_key = "key", grant_type = "api_key", client_id = "clientId"))
                    .take(3)
        }
                /* Then */
                .expectSubscription()
                .assertNext { assertThat(it.access_token).isEqualTo("1st") }
                .thenAwait(ofSeconds(5))
                .assertNext { assertThat(it.access_token).isEqualTo("2nd") }
                .thenAwait(ofSeconds(15))
                .assertNext { assertThat(it.access_token).isEqualTo("3rd") }
                .verifyComplete()
    }

    @Test
    @Timeout(5)
    fun `should work with replay on sub flux`() {
        /* Given */
        val tokenProvider = TokenProvider("foo", "clientId", client)

        val tokenFlux = tokenProvider
                .fetchTokenWithApiKey(AuthInformation(api_key = "key", grant_type = "api_key", client_id = "clientId"))
                .log("tokenFlux")
                .replay(1)

        tokenFlux.connect()

        /* When */
        StepVerifier.withVirtualTime { tokenFlux.take(3) }
                /* Then */
                .expectSubscription()
                .assertNext { assertThat(it.access_token).isEqualTo("1st") }
                .thenAwait(ofSeconds(5))
                .assertNext { assertThat(it.access_token).isEqualTo("2nd") }
                .thenAwait(ofSeconds(15))
                .assertNext { assertThat(it.access_token).isEqualTo("3rd") }
                .verifyComplete()
    }

    @Test
    fun `should work on replay`() {
        /* Given */
        val flux = Flux.just(1, 2, 3)
                .log()
                .replay(1)
        /* When */
        flux.connect()
        /* Then */
        StepVerifier.create(Mono.from(flux))
                .expectSubscription()
                .expectNext(3)
                .verifyComplete()
    }

}
