package com.gitlab.davinkevin.issue.projectreactor.issueafterupgradeto332

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class IssueAfterUpgradeTo332Application

fun main(args: Array<String>) {
	runApplication<IssueAfterUpgradeTo332Application>(*args)
}
